# Golang Client for Amazon Product Advertising API

## A client library for the Amazon Product Advertising API 

## Installation
  
```
 go get -u https://gitlab.com/fooooo/goAmazonAdvertisingClient
```

## Summary

This library searches Amazon's Product Advertising API for Browse Nodes by ID and Products by ASIN. It was pieced together mostly from the libraries listed below, and stripped down to have no external, non-standard library dependencies.  

Then I added Gobuffalo envy! It just makes environment variables easier to handle. Putting your secret values (API keys, secret keys, login info, etc) into environment variables is a good practice, as long as you keep your private info private.  Be sure .env is ignored in your .gitignore file!

## Usage

First install as shown above. This library relies on the following values in order to run:
* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY
* AWS_HOST
* AWS_ASSOCIATE_TAG

The easiest way to set these is to edit the included .env file. Then call the library from your go app and handle the results however you wish.

## Example

![Carbon.svg Example code from Buffalo](./carbon.png)
[Link](https://carbon.now.sh/?bg=rgba(171,%20184,%20195,%201)&t=seti&wt=none&l=auto&ds=true&dsyoff=20px&dsblur=68px&wc=true&wa=true&pv=48px&ph=32px&ln=false&fm=Hack&fs=14px&si=false&code=package%2520actions%250A%250Aimport%2520(%250A%2509%2522github.com%252Fgobuffalo%252Fbuffalo%2522%250A%2509amzn%2520%2522gitlab.com%252Ffooooo%252FgoAmazonAdvertisingClient%2522%250A)%250A%250A%252F%252F%2520HomeHandler%2520is%2520a%2520default%2520handler%2520to%2520serve%2520up%250A%252F%252F%2520a%2520home%2520page.%250Afunc%2520HomeHandler(c%2520buffalo.Context)%2520error%2520%257B%250A%2509wiiProds%2520%253A%253D%2520amzn.GetProdsForNode(%252214218901%2522)%250A%2509c.Set(%2522wiiProds%2522%252C%2520wiiProds)%250A%2509return%2520c.Render(200%252C%2520r.HTML(%2522index.html%2522))%250A%257D%250A&es=2x&wm=false)

### Thanks!

* https://github.com/DDRBoxman/go-amazon-product-api
* https://github.com/ngs/go-amazon-product-advertising-api
* https://github.com/gobuffalo/envy