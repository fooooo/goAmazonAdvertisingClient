package goAmazonAdvertisingClient

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"
)

func (api AmazonProductAPI) GenSignAndFetch(Operation string, Parameters map[string]string) (string, error) {
	genURL, err := GenerateAmazonURL(api, Operation, Parameters)
	if checkError(err) == true {
		return "", err
	}

	SetTimestamp(genURL)

	signedurl, err := SignAmazonURL(genURL, api)
	if err != nil {
		if checkError(err) == true {
			return "", err
		}
		return "", err
	}

	if api.Client == nil {
		api.Client = http.DefaultClient
	}

	resp, err := api.Client.Get(signedurl)
	if checkError(err) == true {
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if checkError(err) == true {
		return "", err
	}

	return string(body), nil
}

func GenerateAmazonURL(api AmazonProductAPI, Operation string, Parameters map[string]string) (finalURL *url.URL, err error) {

	result, err := url.Parse(api.Host)
	if checkError(err) == true {
		return nil, err
	}

	result.Host = api.Host
	result.Scheme = "http"
	result.Path = "/onca/xml"

	values := url.Values{}
	values.Add("Operation", Operation)
	values.Add("Service", "AWSECommerceService")
	values.Add("AWSAccessKeyId", api.AccessKey)
	values.Add("Version", "2013-08-01")
	values.Add("AssociateTag", api.AssociateTag)

	for k, v := range Parameters {
		values.Set(k, v)
	}

	params := values.Encode()
	result.RawQuery = params

	return result, nil
}

func SetTimestamp(origURL *url.URL) (err error) {
	values, err := url.ParseQuery(origURL.RawQuery)
	if checkError(err) == true {
		return err
	}
	values.Set("Timestamp", time.Now().UTC().Format(time.RFC3339))
	origURL.RawQuery = values.Encode()

	return nil
}

func SignAmazonURL(origURL *url.URL, api AmazonProductAPI) (signedURL string, err error) {

	escapeURL := strings.Replace(origURL.RawQuery, ",", "%2C", -1)
	escapeURL = strings.Replace(escapeURL, ":", "%3A", -1)

	params := strings.Split(escapeURL, "&")
	sort.Strings(params)
	sortedParams := strings.Join(params, "&")

	toSign := fmt.Sprintf("GET\n%s\n%s\n%s", origURL.Host, origURL.Path, sortedParams)

	hasher := hmac.New(sha256.New, []byte(api.SecretKey))
	_, err = hasher.Write([]byte(toSign))
	if checkError(err) == true {
		return "", err
	}

	hash := base64.StdEncoding.EncodeToString(hasher.Sum(nil))

	hash = url.QueryEscape(hash)

	newParams := fmt.Sprintf("%s&Signature=%s", sortedParams, hash)

	origURL.RawQuery = newParams

	return origURL.String(), nil
}
