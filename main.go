package goAmazonAdvertisingClient

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"os"
)

var api = config()

//GetProdsForNode will get the products for the given browse node id as []AmazonItem
func GetProdsForNode(browseNodeID string) []AmazonItem {
	browseNode := handleBrowseResponse(api.BrowseNodeLookupWithResponseGroup(browseNodeID, "BrowseNodeInfo,TopSellers"))
	prods := make([]AmazonItem, 0)
	for _, node := range browseNode.ChildNodes {
		prods = append(prods, node.Products...)
	}

	return prods
}

func config() AmazonProductAPI {
	var api AmazonProductAPI
	api.AccessKey = os.Getenv("AWS_ACCESS_KEY_ID")
	api.SecretKey = os.Getenv("AWS_SECRET_ACCESS_KEY")
	api.Host = os.Getenv("AWS_HOST")                  //host and associate tags aren't really secret
	api.AssociateTag = os.Getenv("AWS_ASSOCIATE_TAG") //but keeping them in .env makes it easy to change

	api.Client = &http.Client{}
	return api
}

func handleBrowseResponse(result string, err error) AmazonBrowseNodeLookupResponse {
	checkError(err)
	rslt := []byte(result)
	var y AmazonBrowseNodeLookupResponse

	err = unMarshalIt(rslt, &y)

	checkError(err)

	return y
}

func handleSearchResponse(result string, err error) []AmazonItem {
	checkError(err)
	rslt := []byte(result)
	var y AmazonItemSearchResponse
	err = unMarshalIt(rslt, &y)

	checkError(err)

	return y.AmazonItems.Items
}

func unMarshalIt(b []byte, result interface{}) error {
	err := xml.Unmarshal(b, result)
	if checkError(err) == true {
		return err
	}
	return nil
}

func checkError(err error) bool {
	if err != nil {
		fmt.Println(err)
		return true
	}
	return false
}

/*
ItemLookup takes a product ID (ASIN) and returns the result (string, error)
*/
func (api AmazonProductAPI) ItemLookup(ItemID string) (string, error) {
	params := map[string]string{
		"ItemId":        ItemID,
		"ResponseGroup": "Images,ItemAttributes,Small,EditorialReview",
	}

	return api.GenSignAndFetch("ItemLookup", params)
}

/*
BrowseNodeLookup takes a BrowseNodeId and returns the result. (string, error)
*/
func (api AmazonProductAPI) BrowseNodeLookup(nodeID string) (string, error) {
	params := map[string]string{
		"BrowseNodeId": nodeID,
	}
	return api.GenSignAndFetch("BrowseNodeLookup", params)
}

// BrowseNodeLookupWithResponseGroup takes a BrowseNodeID and a response group (or CSV of groups) and returns a string, error
func (api AmazonProductAPI) BrowseNodeLookupWithResponseGroup(nodeId string, responseGroup string) (string, error) {
	params := map[string]string{
		"BrowseNodeId":  nodeId,
		"ResponseGroup": responseGroup,
	}
	return api.GenSignAndFetch("BrowseNodeLookup", params)
}
